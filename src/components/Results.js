import React, {Component} from "react";
import {render} from "react-dom";

export default class Result extends Component {

    render(){
        return (
            <div className="text-center">
                {this.props.count} &nbsp;
                {this.props.count === 0 ? "résultat" :
                this.props.count === 1 ? "résultat trouvé":
                "résultats trouvés"} pour {this.props.job} en {this.props.department}
            </div>
        )
    }
}

