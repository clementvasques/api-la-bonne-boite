import React, {Component} from "react";

class List extends Component{

    render() {
        return (
            <div className="d-flex justify-content-center">
                <div className="card mb-3 w-75">
                    <h5 className="card-header">{this.props.name}</h5>
                    <div className="card-body">
                        <div className="card-text">
                            <div><span className="text-primary">Adresse : </span>{this.props.adress}</div>
                            <div><span className="text-primary">Score de recrutement : </span>{this.props.stars}</div>
                            <div><span className="text-primary">Type de contact : </span>{this.props.contact}</div>
                            <div><span className="text-primary">Site web : </span><a href={this.props.website}>{this.props.website}</a></div>
                            <div><span className="text-primary">Taille : </span>{this.props.taille}</div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default List