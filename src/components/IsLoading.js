import React, {Component} from "react";

class IsLoading extends Component {
    render() {
        return (
            <div className="d-flex justify-content-center w-100">
                <div className="spinner-border " role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        )
    }
}

export default IsLoading