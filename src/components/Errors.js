import React, {Component} from "react";

class Errors extends Component {
    render() {
        return (
            <div className="text-danger p-5">
                {this.props.errors}
            </div>
        )
    }
}

export default Errors