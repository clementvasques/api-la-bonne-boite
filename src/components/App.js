import React from "react";
import Search from "./Search"
import Result from "./Results";
import IsLoading from "./IsLoading";
import List from "./List"
import Errors from "./Errors";

// import { createControlComponent } from '@react-leaflet/core'
// import { Control } from 'leaflet'
//
// export const ZoomControl = createControlComponent(
//     (props) => new Control.Zoom(props),
// )
// import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'


class App extends React.Component {

    state = {
        nbResults: null,
        department: "",
        job: "",
        isLoading : null,
        data: null,
        errors:null,

        name: null,
        taille: null,
        adress: null,
        contact: null,
        stars: null,
        website: null,
        alternance:null,
        latitude:null,
        longitude:null
    }

    handleSearchForm = async (newDepartment, newJob) =>  {
        // met à jour l'état quand le formulaire est soumis
        this.setState({
            department: newDepartment,
            job: newJob,
            isLoading: true
        })


        const response = await fetch ("http://labonneboite.gupi3944.odns.fr/company?rome_codes_keyword_search="+ newJob + "&departments=" + newDepartment)

        if (response.status === 200){
            const body = await response.json()

            this.setState({
                nbResults: body.companies_count,
                isLoading: false,
                data: body,
                errors: null
            })

        } else {
            if (response.status=== 400){
                this.setState({
                    isLoading: null,
                    errors: "Votre requête n'est pas valide, veuillez l'écrire avec la bonne orthographe"
                })
            }
            if (response.status=== 401){
                this.setState({
                    isLoading: null,
                    errors: "Vous n'êtes pas autorisé à accéder à ce serveur"
                })
            }
        }
    }



    render() {

        return(
            <div className=''>
                <h1 className="card text-center p-2 text-primary">
                    La bonne boite
                </h1>
                <br/>
                <br/>

                {/*// onSubmit vient en fait de la fonction this.props.onSubmit, on fait ça pour faire remonter l'info de l'enfant et le fournir à updateSearchForm*/}
                <Search onSubmit={this.handleSearchForm}/>
                <br/>
                {this.state.isLoading === true ? <IsLoading /> :
                    this.state.isLoading === false ?
                    <Result count={this.state.nbResults} job={this.state.job} department={this.state.department}/> : null
                }
                <br/>
                {this.state.isLoading === false ?
                    (this.state.data.companies.map(function (entry){
                        const adress = entry.address
                        const stars = entry.stars
                        const contact = entry.contact_mode
                        const website = entry.website
                        const name = entry.name
                        const taille = entry.headcount_text

                        console.log(entry)

                        return (
                            <List adress={adress}
                                  stars={stars}
                                  contact={contact}
                                  website={website}
                                  name={name}
                                  taille={taille}
                            />
                        )
                        })) : null
                }
                {this.state.errors ?
                    <Errors errors={this.state.errors} /> : null
                }

            </div>
        )
    }
}

export default App