import React, {Component} from "react";
import PropTypes from 'prop-types';

class Search extends Component{
    state = {
        // this.props.department vient en fait du state de App
        department: this.props.department,
        job: "full stack"
    }

    render() {
        return (
            <form className="" onSubmit={(event => {
                event.preventDefault()
                this.props.onSubmit(this.state.department, this.state.job)
            })}>
                <div className="container d-flex justify-content-center">
                    <label className="text-center" htmlFor="">Donnez-nous votre métier</label>
                </div>
                <div className="container d-flex justify-content-center">
                    <input className="w-75" type="text" value={this.state.job} onChange={ (event) => {this.setState({job : event.target.value})} }/>
                </div>
                <br/>
                <div className="container d-flex justify-content-center">
                    <label htmlFor="">C'est vers où ?</label>
                </div>
                <div className="container d-flex justify-content-center">
                    <input className="w-75" type="text" value={this.state.department} onChange={ (event) => {this.setState({department : event.target.value})} }/>
                </div>
                <br/>
                <div className="container d-flex justify-content-center">
                    <button type="submit"
                            className="btn btn-primary"
                    >Envoyer</button>
                </div>
            </form>
        )
    }
}

Search.propsTypes = {
    onSubmit: PropTypes.function
}

export default Search